import { Button, Container, ButtonGroup } from 'react-bootstrap'
import React from 'react'
import { Link, useHistory, useRouteMatch } from 'react-router-dom'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Movie from './Movie';
import Animation from './Animation';

export default function Video() {

    let {  url } = useRouteMatch();

    return (
        <Container>
            <h1>Video</h1>
            <ButtonGroup aria-label="Basic example">
                <Button variant="secondary" as={Link} to={`${url}/movie`}>Movie</Button>
                <Button variant="secondary" as={Link} to={`${url}/animation`}>Animation</Button>
                
            </ButtonGroup>
            <Switch>
                <Route path={`${url}/movie`} component={Movie} />
                <Route path={`${url}/animation`} component={Animation} />
            </Switch>

        </Container>
    )


}
