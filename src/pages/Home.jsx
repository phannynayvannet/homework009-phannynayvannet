import React from 'react'
import { Button, Card, Container, Row, Image, Col } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom'

export default function Home(props) {
    const history = useHistory()
    return (
        <Container >
            <Row>
                {props.datas.map((item, idx) =>
                    <Col style={{ padding: "20px" }}>
                        <Card className="mr-auto" style={{ width: "300px" }} key={idx}>
                            <Image variant="top" src={item.img} height="200px" />
                            <Card.Body>
                                <Card.Title>{item.title}</Card.Title>
                                <Card.Text>
                                    {item.content}
                                </Card.Text>
                                <Link to={`/detail/${idx+1}`}>
                                    <Button>Read</Button>
                                </Link>
                            </Card.Body>
                        </Card>
                    </Col>
                )
                }
            </Row>
        </Container>
    )
}
