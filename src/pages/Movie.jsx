import { Button,ButtonGroup } from 'react-bootstrap'
import React from 'react'
import {  useLocation,Link, useRouteMatch } from 'react-router-dom'
import query from  'query-string';
export default function Video() {
    
   const location = useLocation();
   const query = new URLSearchParams(location.search)
   const data = query.get("name")
    let {  url } = useRouteMatch();
    console.log(data);
    return (
    
        <div>
            <h1>Movie Category</h1>
            <ButtonGroup aria-label="Basic example">
                <Button variant="secondary"  as={Link} to={`${url}?type=adventure`}>Adventure</Button>
                <Button variant="secondary" as={Link} to={`${url}?type=crime`}>Crime</Button>
                <Button variant="secondary" as={Link} to={`${url}?type=action`}>Action</Button>
                <Button variant="secondary" as={Link} to={`${url}?type=romance`}>Romance</Button>
                <Button variant="secondary" as={Link} to={`${url}?type=comedy`}>Comedy</Button>
            </ButtonGroup>
            <h1>Please Choose Category:<h1 style={{color:'red'}}> {data} </h1> </h1>
        </div>
    )


}
