import React from 'react'
import Welcome from './Welcome';
import  {  useState } from 'react'
import { Redirect } from 'react-router';
export default function Protected(props) {

    if(props.test){
        return <Welcome/>
    }else
    
    return (
        <Redirect to="/auth"/>
    )
}
