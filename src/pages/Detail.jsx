
import React from 'react'
import { Container} from 'react-bootstrap'
import { useParams } from 'react-router-dom'

export default function Detail() {
    let param = useParams()
    return (
        <Container>
            <h1 >Detail: ID = {param.id}</h1>
        </Container>
    )
}






