import React from 'react'
import { Button, Container, Row, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function welcome(props) {

    return (
        <Container>
            <h1>Welcome</h1>
                <Button variant="primary" type="button" onClick={props.onLogs} >
                    Log out
                </Button>
            </Container>
    )
}
