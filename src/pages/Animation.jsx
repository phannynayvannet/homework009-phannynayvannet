import { Button, Container, ButtonGroup } from 'react-bootstrap'
import React from 'react'
import { Link,  useRouteMatch,useLocation} from 'react-router-dom'
export default function Video() {

    const location = useLocation();
   const query = new URLSearchParams(location.search)
   const name = query.get("name")
   console.log(name);
    return (
        <div>
            <h1>Animation Category</h1>
            <ButtonGroup aria-label="Basic example">
                <Button variant="secondary" as={Link} to="/video/animation?type=action">Action</Button>
                <Button variant="secondary" as={Link} to="/video/animation?type=romance">Romance</Button>
                <Button variant="secondary" as={Link} to="/video/animation?type=comedy">Comedy</Button>
            </ButtonGroup>
            <h1>Please Choose Category:<h1 style={{color:'red'}}> {name} </h1> </h1>
        </div>
    )


}
