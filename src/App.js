
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './components/Menu';
import React, { Component, useState } from 'react'
import Home from './pages/Home';
import Welcome from './pages/Welcome';
import Movie from './pages/Movie';
import Auth from './pages/Auth';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Account from './pages/Account';
import Detail from './pages/Detail';
import Video from './pages/Video';
import Protected from './pages/Protected';

export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: 
        [
        {
          //id:1,
          img: "https://th.bing.com/th/id/OIP.WRX9rs_5WEDDg6qikkRS1gHaEk?w=272&h=180&c=7&o=5&pid=1.7",
          title: "Apple",
          content: "content",
        },
        {
          //id:2,
          img: " https://th.bing.com/th/id/OIP.VjGVrPBCO_OhgNwBjPyX-AHaE8?w=246&h=180&c=7&o=5&pid=1.7",
          title: "Banana",
          content: "content",
        },
        {
          //id:3,
          img: "https://th.bing.com/th/id/OIP.9v4TXdij23klxirbdPBYtgHaFj?w=232&h=180&c=7&o=5&pid=1.7",
          title: "Orage",
          content: "content",
        },
        {
          //id:4,
          img: "https://th.bing.com/th/id/OIP.S1IBCkWyLM3EPsZCWm0hfwHaFS?w=275&h=197&c=7&o=5&pid=1.7",
          title: "Grape",
          content: "content",
        },
        {
          //id:5,
          img: "https://th.bing.com/th/id/OIP.1MFOJHFWb3pcUGTpgOfizwHaE8?w=291&h=195&c=7&o=5&pid=1.7",
          title: "Coconut",
          content: "content",
        },
        {
          //id:6,
          img: "https://th.bing.com/th/id/OIF.6NSEwlGrXL5ubq2721tjog?w=326&h=183&c=7&o=5&pid=1.7",
          title: "Mango",
          content: "content",
        }
      ],
      test:false
    }
  }

  onLogs=()=>{
   if(this.state.test===false){
    this.setState({
      test:true
    })
  }else{
      this.setState({
        test:false
      })
    }
  }
  
 
  render() {
    return (
      <div>
        <Router>
          <Menu />
          <Switch>
            <Route exact path="/">

              <Home datas={this.state.data} />

            </Route>
            <Route path="/video" component={Video} />
            <Protected path="/welcome" component={Welcome} test={this.state.test} onLogs={this.onLogs}/>
            <Route path="/account" component={Account} />
            <Route path="/detail/:id" component={Detail} />
            <Route path="/Auth" render={() => <Auth onLogs={this.onLogs}/>}  />
            <Route path="/video/movie" component={Movie} />
          </Switch>

        </Router>
      </div>
    )

  };
}


