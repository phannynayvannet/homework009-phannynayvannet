import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav,Form,Button,Navbar,FormControl} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Menu(props) {
     const [id,SetId] = useState(30)
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="/">React-Router</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    <Nav.Link as={Link} to="/video">Video</Nav.Link>
                    <Nav.Link as={Link} to="/account">Account</Nav.Link>
                    <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
                    <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-info">Search</Button>
                </Form>
            </Navbar>
        </div>
    )
}
